///Theory
// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval
/*setTimeout Это метод встроенный в браузер, который принемает два параметра 1) callBack function,
2) delay - время через которое
* выполница функция коллБэк, выполнится она один раз*/
/*setInterval Это тоже встроенынй метод и принемает теже параметры что и setTimeout, но выполняется бесконечное колиство раз,
если его не остановить, остановить их можно спомощью функций clearInterval() и clearTimeout()*/

// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Она сработает, но не сразу так, как попадет в очередь задач.

// 3. Чому важливо не забувати викликати функцію clearInterval(),
// коли раніше створений цикл запуску вам вже не потрібен?
// Потому, что браузер будет перегружен.

/// Practice

let buttonStop = document.createElement("button");
buttonStop.innerText = "stop";
document.body.prepend(buttonStop);

let buttonStart = document.createElement("button");
buttonStart.innerText = "start";
document.body.prepend(buttonStart);

let timerText = document.createElement("span");
let container = document.getElementById("container")
container.appendChild(timerText);

let images = document.querySelectorAll("img");

let delay = 3000;
let timerId = null;
let timerInterval = null;
let lastImg = null;
let count = 0;
let currentSec = null;
let currentMs = null;

showImages();

function showImages() {

    if (count === images.length) {
        count = 0;
    }
    if (count <= images.length) {
        if (images[count].classList.contains("hide")) {
            images[count].classList.add("show");
            images[count].classList.remove("hide");
        }

        if (lastImg !== null && lastImg.classList.contains("show")) {
            lastImg.classList.add("hide");
            lastImg.classList.remove("show");
        }
        lastImg = images[count];
        count++;
    }
    timer(delay);
    timerId = setTimeout(showImages, delay);
}

function timer(delay) {
    let timerCurrent = delay;
    let timeNow = new Date();
    let timeEnd = new Date(Date.parse(timeNow.toString()) + timerCurrent);

    timerInterval = setInterval(() => {
        let timeNow = new Date();
        let differenceTime = timeEnd.getTime() - timeNow.getTime();

        if (differenceTime < 0) {
            clearInterval(timerInterval);
            return;
        }
        currentSec = Math.floor((differenceTime / 1000) % 60);
        currentMs = (differenceTime - currentSec * 1000);

        timerText.innerText = `Timer: ${currentSec}:${currentMs.toString()}`;
    }, 1)
}

buttonStop.addEventListener("click", () => {
    clearTimeout(timerId);
    clearInterval(timerInterval);
    timerInterval = null;
    timerId = null;
});

buttonStart.addEventListener("click", () => {

    if (timerId === null&& timerInterval===null) {
        showImages(count);
    }
});
